# README #

### What is this repository for? ###

* Monitor CertCenter Certificates via it's API

### How do I get set up? ###

* Install Python v2.7

* Install CertCenter package with ```pip install CertCenter```

* Create folder ```/home/cc-expiry-monitor/conf```

* Create folder ```/home/cc-expiry-monitor/state```

* Put Python script in ```/home/cc-expiry-monitor/```

* Add example configuration files in folder ```/home/cc-expiry-monitor/\conf\```

* Change configuration files to your needs

### Example cc-auth.json ###
```
{
    "cc_bearer": "EXAMPLE.oauth2.certcenter.com"
}
```

### Example expmon-config.json ###
```
{
    "tag_dont_monitor": "DONT_MONITOR",
    "tag_dont_replace": "DONT_REPLACE",
    "expire_lead_days": 60,
    "notification_to": "you@yourdomain.tld",
    "notification_from": "some@yourdomain.tld",
    "smtp_server": "your.smtp.server.com",
    "smtp_starttls": 1,
    "smtp_port": 587,
    "smtp_username": "myusername",
    "smtp_password": "mypassword"
}
```

### Troubleshooting
* tbd

### Todo

* Reduce time a bearer token is valid and implement refresh token logic
