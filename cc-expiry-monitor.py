#!/opt/local/bin/python
#-*- coding: utf-8 -*-

import logging
logging.basicConfig(format='%(levelname)s %(asctime)s   %(message)s', level=logging.DEBUG)

import os
import io
import sys
import json
import datetime
import unicodedata
import CertCenter
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

#####################################################################

FILE_CC_AUTH = "conf/cc-auth.json"                             # path and filename to auth file
FILE_CONFIG = "conf/expmon-config.json"                        # path and filename to config file
FILE_NOTIFIED = "state/notified-list.json"                     # path and filename to notify list

CCDTFORMAT = "%Y-%m-%dT%H:%M:%SZ"

NOTIFY_TYPE_EXPIRE = 1
NOTIFY_TYPE_REPLACE = 2

ENCODING = "utf-8"
NOW = datetime.datetime.now()

#####################################################################

def json_load(filename, ignoreErrors = False):
    try:
        file = io.open(filename, "r", encoding=ENCODING)
        data = json.load(file)
        file.close()
        return data
    except Exception as e:
        if not ignoreErrors:
            logging.error("JSON Load Error: " + str(e))
            sys.exit(1)

def json_write(filename, data):
    try:
        cf = io.open(filename, "w+", encoding=ENCODING)
        cf.write(unicode(json.dumps(data, indent=4, ensure_ascii=False)))
        cf.close()
    except Exception as e:
        logging.error("JSON Write Error: " + str(e))
        return False
    return True

def cc_api_certlist():
    query = { "Status":                   "COMPLETE",
              "ProductType":              "SSL,SMIME",
              "Page":                     0,
              "ItemsPerPage":             10000,
              "OrderBy":                  "TS_VALIDITY_ENDE",
              "OrderDir":                 "ASC",
              "filterScheduledToReplace": False,
              "includeFulfillment":       False,
              "includeOrderParameters":   True,
              "includeBillingDetails":    True,
              "includeContacts":          True,
              "includeOrganizationInfos": True,
              "includeVettingDetails":    False,
              "includeDCVStatus":         False
            }

    api = CertCenter.CertAPI()
    api.setBearer(json_load(FILE_CC_AUTH)["cc_bearer"])
    res = api.Orders(req = query)
    if not res or res["success"] == False:
        logging.error("CC API Error: " + str(res["ErrorId"]) + ": " + res["Message"] + ". Quitting...")
        sys.exit(1)
    return res

def send_email(mfrom, mto, msubject, mbody):
    try:
        msg = MIMEMultipart()
        msg['From'] = mfrom
        msg['To'] = mto
        msg['Subject'] = msubject
        msg.attach(MIMEText(mbody, 'plain'))

        smtpsrv = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        if SMTP_TLS:
            smtpsrv.starttls()
        smtpsrv.ehlo()
        smtpsrv.login(str(SMTP_USER), str(SMTP_PASS))
        res = smtpsrv.sendmail(mfrom, mto, msg.as_string())
        smtpsrv.quit()
    except Exception as e:
        logging.error("SMTP Error: " + str(e))
        return False
    return True

def parse_cc_datetime(expdt):
    return datetime.datetime.strptime(expdt, CCDTFORMAT)

def has_tag(cert, tag):
    return False

def remove_expired_notified():
    notexpired = {}
    notified = json_load(FILE_NOTIFIED, ignoreErrors = True)
    if notified:
        for certid, cert in notified.iteritems():
            end_date = parse_cc_datetime(cert["EndDate"])
            expire_in_days = (end_date - NOW).days
            if expire_in_days >= -EXPIRE_LEAD_DAYS:
                notexpired[cert['OrderID']] = cert
            else:
                logging.info("Removed certificate " + cert["CommonName"] + " #" + str(cert["OrderID"]) + " from notified list, it expired " + str(expire_in_days*-1) + " days ago")
        json_write(FILE_NOTIFIED, notexpired)

def was_notified(orderid):
    notified = json_load(FILE_NOTIFIED, ignoreErrors = True)
    if notified and str(orderid) in notified:
        return notified[str(orderid)]
    return None

def set_notified(cert, notifytype):
    notifydate = NOW.strftime(CCDTFORMAT)
    jdata = { "OrderID": cert["CertCenterOrderID"],
              "NotifyType": notifytype,
              "NotifyDate": notifydate,
              "CommonName": cert["CommonName"],
              "StartDate": cert["OrderStatus"]["StartDate"],
              "EndDate": cert["OrderStatus"]["EndDate"]
            }

    notified = json_load(FILE_NOTIFIED, ignoreErrors = True)
    if not notified:
        notified = {}
    notified[jdata['OrderID']] = jdata
    return json_write(FILE_NOTIFIED, notified)

def send_notification(cert, notifytype):
    if notifytype == NOTIFY_TYPE_EXPIRE:
        action = u"Ablauf"
    elif notifytype == NOTIFY_TYPE_REPLACE:
        action = u"Austausch"

    startdate = parse_cc_datetime(cert["OrderStatus"]["StartDate"])
    enddate = parse_cc_datetime(cert["OrderStatus"]["EndDate"])
    subject = action + " des digitalen Zertifikats " + cert["CommonName"]
    message = u"============================================================\n" \
              u"Automatisch generierte Meldung\n" \
              u"============================================================\n\n" \
              u"" + action + " des digitalen Zertifikats " + cert["CommonName"] + "\n\n" \
              u"Bestell Nummer\n" + str(cert["CertCenterOrderID"]) + "\n\n" \
              u"Produkt\n" + cert["OrderParameters"]["ProductCode"] + "\n\n" \
              u"Gueltigkeitsdauer\n" + str(cert["OrderParameters"]["ValidityPeriod"]) + " Monate" + "\n\n" \
              u"Start Datum\n" + startdate.strftime("Am %d.%m.%Y um %H:%M") + "\n\n" \
              u"Ablauf Datum\n" + enddate.strftime("Am %d.%m.%Y um %H:%M") + "\n\n" \
              u"============================================================\n"

    send_email(NOTIFICATION_FROM, NOTIFICATION_TO, subject, message)
    set_notified(cert, notifytype)

#####################################################################
# Read config
#####################################################################

config_data = json_load(FILE_CONFIG)
TAG_DONT_MONITOR = config_data["tag_dont_monitor"]             # If tag present, no alert upon expiry
TAG_DONT_REPLACE = config_data["tag_dont_replace"]             # If tag present, no alert for replacing cert
EXPIRE_LEAD_DAYS = config_data["expire_lead_days"]             # Days to send notification before expiration
NOTIFICATION_TO  = config_data["notification_to"]              # Where to send notification emails to
NOTIFICATION_FROM  = config_data["notification_from"]          # Sender address of notification
SMTP_SERVER = config_data["smtp_server"]                       # Hostname of outgoing SMTP
SMTP_TLS = config_data["smtp_starttls"]                        # Wether to use TLS
SMTP_PORT = config_data["smtp_port"]                           # Port of outgoing SMTP
SMTP_USER = config_data["smtp_username"]                       # Username of outgoing SMTP
SMTP_PASS = config_data["smtp_password"]                       # Password of outgoing SMTP

#####################################################################
# Part 1: Cleanup expired certificates from notified list
#####################################################################

remove_expired_notified()

#####################################################################
# Part 2: Check certificates
#####################################################################

certs = cc_api_certlist()
for cert in certs["OrderInfos"]:
    if has_tag(cert, TAG_DONT_MONITOR):
        logging.info(cert["CommonName"] + " tagged with " + TAG_DONT_MONITOR + " skipping it")
        continue

#    if (cert["scheduledForReplacement"]["eventSymantecDistrust"]["phase1"] == True) or \
#       (cert["scheduledForReplacement"]["eventSymantecDistrust"]["phase2"] == True):
#        if has_tag(cert, TAG_DONT_REPLACE):
#            logging.info(cert["CommonName"] + " #" + str(cert["CertCenterOrderID"]) + " scheduled for replacement, skipping it because tagged with " + TAG_DONT_REPLACE)
#        else:
#            if was_notified(cert["CertCenterOrderID"]):
#                logging.info(cert["CommonName"] + " #" + str(cert["CertCenterOrderID"]) + " scheduled for replacement, already notified")
#            else:
#                logging.info(cert["CommonName"] + " #" + str(cert["CertCenterOrderID"]) + " scheduled for replacement, notifying now")
#                send_notification(cert, NOTIFY_TYPE_REPLACE)

    end_date = parse_cc_datetime(cert["OrderStatus"]["EndDate"])
    expire_in_days = (end_date - NOW).days
    if expire_in_days <= EXPIRE_LEAD_DAYS and expire_in_days >= -EXPIRE_LEAD_DAYS:
        if was_notified(cert["CertCenterOrderID"]):
            logging.info(cert["CommonName"] + " #" + str(cert["CertCenterOrderID"]) + " expires in " + str(expire_in_days) + " days, already notified")
        else:
            logging.info(cert["CommonName"] + " #" + str(cert["CertCenterOrderID"]) + " expires in " + str(expire_in_days) + " days, notifying now")
            send_notification(cert, NOTIFY_TYPE_EXPIRE)

# eof
